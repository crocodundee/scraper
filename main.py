from Parser import Parser
import xmldoc as xml
import time


if __name__ == "__main__":
    titles, url = Parser.get_titles_url("https://rules.sos.ri.gov/organizations")
    for idx, title in enumerate(titles):
        page = Parser(url[idx], idx)
        page.parse()
        xml.create_doc(title, page.path, page.text)
        time.sleep(4)
