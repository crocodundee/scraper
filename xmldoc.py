from time import localtime, strftime
import lxml.etree as doc


def is_text_exist(text):
    """Check if text exist in node"""
    return type(text) == list


def add_child(parent, child, text=None):
    """Create new node to xml-tree"""

    tag = doc.SubElement(parent, child)
    tag.text = text
    return tag


def add_path_tree(root, path_parts, path_end):
    """Add level header to doc"""

    path = ""
    for p in path_parts:
        path = path + p
        level = doc.SubElement(root, 'level', path=path)
        add_child(level, 'heading', p)
        path = path + "\\"
        root = level
    level = doc.SubElement(root, 'level', path=path + path_end)
    return level


def add_text(parent, text):
    """Add text content of doc-node"""

    content = doc.SubElement(parent, 'text')
    add_child(content, 'p', text[1][0])


def citation_prettify(text):
    """Clear crawled citation string from whitespace"""

    text = list(text)
    capitalize = [text.index(alpha) for alpha in text if alpha.isalpha() and alpha.isupper()][0]
    text = "".join(text)
    cit_number = text[:capitalize].strip()
    cit_value = text[capitalize:].strip()
    return cit_number, cit_value


def add_content(parent, header, text):
    """Add full created node to xml-doc via type"""

    if is_text_exist(text):
        parent.set('text_exist', 'True')
        add_child(parent, 'citation', "Citation " + header[0])
        add_child(parent, 'heading', header[1])
        add_text(parent, text)
    else:
        parent.set('text_exist', 'False')
        content = doc.SubElement(parent, 'text')
        add_child(content, 'p', text)


def create_doc(doc_name, text_path, text):
    """Create xml-document for crawled title"""

    body = doc.Element('document')
    tree = doc.ElementTree(body)

    add_child(body, 'taskname', 'Introlab-Systems test task')
    add_child(body, 'workername', 'Anastasiia Didan')
    add_child(body, 'date', strftime("%Y-%m-%d %H:%M:%S", localtime()))
    cit_number, cit_value = citation_prettify(text[0][0])
    level = add_path_tree(body, text_path, cit_value)
    add_content(level, [cit_number, cit_value], text)

    tree.write("title/" + doc_name, pretty_print=True, xml_declaration=True, encoding="utf-8")
