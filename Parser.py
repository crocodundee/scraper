import requests as req
from bs4 import BeautifulSoup as html


class Parser(object):
    """Data crawling class"""

    def __init__(self, url, title_ind):
        self.url = url
        self.path = [self.get_titles_def("https://rules.sos.ri.gov/organizations")[title_ind]]
        self.text = []
        self.text_exist = True

    @classmethod
    def get_titles_url(cls, home):
        """Get all titles with their root url"""

        soup = cls.set_soup_object(home)
        trows = soup.select('tr', {'role': 'row'})
        links = [tr.a for tr in trows if tr.a is not None and tr.a.get_text() != '']
        titles_url = {l.get_text().strip(): l['href'] for l in links}
        return list(titles_url.keys()), list(titles_url.values())

    @staticmethod
    def set_soup_object(url):
        """Create soup-object for page crawling"""

        header = {'UserAgent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0'}
        text = req.get(url, headers=header).content.decode('cp1251')
        soup = html(text, 'html.parser')
        return soup

    @staticmethod
    def is_text_exist(soup):
        """Check if title information exist on current page"""

        if soup.find('div', {'class': 'tabs-panel'}):
            return True
        elif soup.find('table', {'class': 'unstriped'}):
            return False
        else:
            return 'NO_PARTS'

    def get_regulation_text(self, url):
        """Get item regular text as main document content"""

        soup = self.set_soup_object(url)
        headers = soup.select('h1', {'class': 'ricr-heading-1-western'})
        paragraph = soup.select('section', {'class': 'doc-block'})
        text = {headers[i + 1].text: paragraph[i].text for i in range(len(paragraph))}
        self.text = [list(text.keys()), list(text.values())]

    def get_titles(self, url):
        """Get item subtitles"""

        soup = self.set_soup_object(url)
        trows = soup.find_all('tr', {'class', 'odd-parent'})
        td_base_width = [tr.find_all('td') for tr in trows][0][1]['width']
        td = [t for t in soup.find_all('td') if t.has_attr('width') and t.attrs['width'] == td_base_width]
        titles = {t.a.text.strip(): t.a['href'] for t in td}
        return titles

    def get_titles_def(self, url):
        """Get titles content"""

        soup = self.set_soup_object(url)
        td = [t for t in soup.find_all('td') if t.has_attr('width')]
        td_width_val = [int(t.attrs['width'][:-1]) for t in td]
        td_width_val_max = max(td_width_val)
        descriptions = [t.text.strip() for (t, width) in zip(td, td_width_val) if width == td_width_val_max]
        return descriptions

    def set_text_path(self, path_part):
        """Set next url to crawl title content"""
        self.path.append(path_part)

    def parse(self):
        """Parse document content"""

        soup = self.set_soup_object(self.url)
        if self.is_text_exist(soup) == 'NO_PARTS':
            no_parts_subchapter = soup.find('div', {'class': 'card-section'}).p.text
            self.text_exist = False
            self.text = no_parts_subchapter
        elif self.is_text_exist(soup):
            self.text_exist = True
            self.get_regulation_text(self.url)
        else:
            title_url = list(self.get_titles(self.url).values())[0]
            title_def = self.get_titles_def(self.url)[0]
            self.url = title_url
            self.set_text_path(title_def)
            self.parse()
